package nl.utwente.di.bookQuote;

import java.util.Map;

public class Quoter {
    private final Map<String, Integer> prices = Map.of("1", 10, "2", 45, "3", 20, "4", 35, "5", 50);

    public double getFarenheit(String celsius){
        return (Double.parseDouble(celsius) * 9) / 5 + 32;
    }
}
