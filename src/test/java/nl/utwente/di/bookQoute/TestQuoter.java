package nl.utwente.di.bookQoute;

import nl.utwente.di.bookQuote.Quoter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestQuoter {

    @Test
    public void testBook1() throws Exception {
        Quoter quoter = new Quoter();
        double price = quoter.getFarenheit("5");
        Assertions.assertEquals(41.0, price, 0.0, "Price of book 1");
    }
}
